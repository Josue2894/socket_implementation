#include "ServerSocket.hpp"
#include "SocketException.hpp"
#include <string>
#include <iostream>

const int kPort = 6336;
const std::string kIp = "127.0.0.1";

int main(int argc, char* argv[])
{
    try
    {
        // Create the socket
        ServerSocket server{kPort};
        std::cout << "Server is running: " << server.get_addr_str()<< ":" << kPort <<std::endl;

        while (true)
        {
            std::cout << "Listening connection: " << server.get_addr_str()<< ":" << kPort <<std::endl;
            ServerSocket new_sock;
            server.Accept(new_sock);

            try
            {
                std::string data;
                // Receive the message
                new_sock >> data;

                std::cout << "Received message: " << data << std::endl;
                // Send the message back
                new_sock << data;
            }
            catch (SocketException &)
            {
                std::cout << "Something is wrong: " << new_sock.get_addr_str() <<std::endl;
            }
        }
    }
    catch (SocketException &e)
    {
        std::cout << "Exception was caught:" << e.description() << "\nExiting.\n";
    }

    return 0;
}